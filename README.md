# Install java jdk before running any java program on CentOS or any Unix/Linux Sidtribution

## Comand to install Java JDK on CentOS 7 
[root@jenkins2 java]# yum install java-1.8.0-openjdk-devel -y

## Set Java version if you have multiple on your server
[root@jenkins2 java]# update-alternatives --config java
 
## Write a simple java Code on your CentOS 7 server using VI Editor

[root@jenkins2 java]# vi myclass.java

class myclass{

    public static void main(String args[]){
    
     System.out.println("Hello World");
     
     System.out.println("This is m y first Java Program");
     
    }
    
}

[root@jenkins2 java]# javac myclass.class

[root@jenkins2 java]# java myclass

### Hello World

### This is m y first Java Program
